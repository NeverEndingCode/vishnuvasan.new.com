---
title: "About"
date: 2020-05-05T01:14:25+05:30
draft: false
tags: ["About"]
showInMenu: true
hideLastModified: true
---
###### May 10, 2020 12:11 PM | **1 Min Read**

*****
Hey Buddy,
Thank you very much for Visiting the Page.Greatly Appreciate your Time!

Welcome to this **Zillionth Zilch** in this Never Ending Useless Web.

## Personally

Trying to be a **better Human @ Heart** out of all the other infinite things I strive to be like being a

- Responsible Son, Loving Husband, Useful Engineer, Budding Developer, Wandering Traveller, Better Listener, Voracious Reader, Avid Thinker
- And Many More Inexplicable/Unexpected Hats I am figuring out in this never ending Continuum!

## Professionally

Like Every Other Indian, After becoming an Engineering Graduate I landed in various jobs which many a times I Hated than Loved

- 10+ Yrs of Experience with [Embedded Systems](https://en.wikipedia.org/wiki/Embedded_system), [Automotive Software](https://en.wikipedia.org/wiki/Automotive_engineering) and funnily [Microsoft Office Suite](https://en.wikipedia.org/wiki/Microsoft_Office)
- Programmed in
	- [C](https://en.wikipedia.org/wiki/C_\(programming_language\)), [VB](https://en.wikipedia.org/wiki/Visual_Basic), [C#](https://en.wikipedia.org/wiki/C_Sharp_\(programming_language\)), [Perl](https://en.wikipedia.org/wiki/Perl), [Python](https://en.wikipedia.org/wiki/Python_\(programming_language\)), [Ruby](https://en.wikipedia.org/wiki/Ruby_\(programming_language\)), [Winwrap](https://en.wikipedia.org/wiki/WinWrap_Basic), [Js](https://en.wikipedia.org/wiki/JavaScript), [Scala](https://en.wikipedia.org/wiki/Scala_\(programming_language\)), [Elm](https://en.wikipedia.org/wiki/Elm_\(programming_language\)) and My Favourite of all [Clojure](https://en.wikipedia.org/wiki/Clojure)
- Landed in Mathematical Model Design with
    - [LogiCAD](https://www.logicals.com/en/logi-cad), [ASCET](https://www.etas.com/en/products/ascet-developer.php), [MATLAB](https://en.wikipedia.org/wiki/MATLAB), [SciLab](https://en.wikipedia.org/wiki/Scilab), [xCos](https://www.scilab.org/software/xcos), [R](https://en.wikipedia.org/wiki/R_\(programming_language\)), [Octave](https://www.gnu.org/software/octave/)
- Worked in Multiple OS Flavors
    - Windows, [Fedora](https://getfedora.org) - My Best, [Debian](https://www.debian.org), [Manjaro](https://manjaro.org), [Ubuntu](https://ubuntu.com), [Kali](https://www.kali.org), [Tails](https://tails.boum.org), [Zorin](https://zorinos.com), [Elementary](https://elementary.io)
- Advocate of Open Source and Privacy. User of
    - [GNOME](https://www.gnome.org) - Ardent Fan :), [KDE](https://kde.org), [XFCE](https://xfce.org), [LXDE](https://lxde.github.io), [EnLightenment](https://www.enlightenment.org)
- Created a Handful of Tools
- Had the Opportunity to work with Quite a Few Intelligent and Extraordinary Minds throughout the career
- Never Loved [Leadership](https://en.wikipedia.org/wiki/Leadership) Role better than the Last 3 Years

Currently in a **[Mid-Life Crisis](https://en.wikipedia.org/wiki/Midlife_crisis)** planning the best possible transition both Professionally and Personally.

> **[Professional Resume](static/Vishnu_Vasan_Nehru_EN_2020_05_05.pdf)**

## Favourite Quote - One among the Many from [Mundasu Kavi](https://en.wikipedia.org/wiki/Subramania_Bharati)

> ###### அச்சம் தவிர்!
> ###### நையப்புடை!
> ###### மானம் போற்று!
> ### ரௌத்திரம் பழகு!
> ###### நேர்பட பேசு!

***
##### PGP Public Key

*****
>mQINBF649FIBEADM1j+mSM7X1+xWLB09q9rTOH8Hfdk5CQFTxFVzTmODvO9FarZa
ldHZZp7tb3f4wOim0plLDjW8fn47nyZjfd7SwWGtfSl+Pc+3PN1zVgi2uCazILVK
tkSs5Xz1qfoczh/PaiOt+23zntAGUeBZl1D8V7/lhhvR6EbHKOt9jyzNFpp1070r
FBO1MwLERabEUAeVqeUV44iFLM7XDcRA4bHqdYwsVUnXLsfNPckLggOJtDk3eXnH
RhV/o9V91cVTzfWtkLPFGeBdJsihis2gHcaiqo2qr3pDspCu7dq2rvCh2NRd6+t5
r33JSzfPCkmT4MOhpxWFg4hVq7w7k1R8O0EVTchmq5IrULbVtm+4qqoTAs0D5JFY
V9dXGmqbKrvER/gbeUld1GM8wjM92P7jKCUM5ekfsLR9OH5OAEMt+pE1wrjpA4wu
Yag4hzgTBL0WYL37w5oVNRNCQsVoS/bOcVTkoPQLMbUxjJoSHDvkBBbgRV5uK3qc
57PXiqMCb7qSSxexigf5/vBPLr9ODivQppvUKY5XjiDn0Ykrt9VjZvOCyuHoGBSs
67MAZfGdeGiKAa8bZTpVy5znDQODEhu5rTtJacYx19Twp1jli5DodS8eI65uYQQK
yIWl0MVdvzpb1+b+XtON9978CFsLB1ltSJ6DISRbApe1TpgBtWoC6rJ7mQARAQAB
tFNWaXNobnUgVmFzYW4gTmVocnUgKFZpc2hudSBWYXNhbiBOZWhydXxjb2RlQHZp
c2hudXZhc2FuLmNvbSkgPGNvZGVAdmlzaG51dmFzYW4uY29tPokCVAQTAQoAPhYh
BK2PaEtAOIBWUmC/Pq3uhcST4gFxBQJeuPRSAhsDBQkB4TOABQsJCAcCBhUKCQgL
AgQWAgMBAh4BAheAAAoJEK3uhcST4gFxM74P/j3qvXv6nvzYAltX6VfZrMZbi27l
o91Ub5yk7A3iVO8aZpkCXl+gtw6FTT7EGwZubP8rRmctNRPp5CeYuzsqeGgq+gDM
7NzxdmjPwvlE1191tf0BHCobPSlbRyMoMR7024InXBxar05bRQw+dhso+bY8+0+A
vOfopooXk27lYyiZFMqlDL/T0fz5mSQTaAp3ThCDF+YDn1AE287Bf1BFZRU7Jxpw
iT2/a4CAhD4rpynbWW1hI3zMaw1/MkdHbQomXU+JeAAXOoVXwUc8UDlVM7KnuxB7
EYP1c4c5VT7fZuxYYqqGeKmsHSY2bZa+bzL90TVvJwg1DLYcQbjcWS0IwC7pdUsZ
yxw+n+J+LNDLvjCQOWArYizrcn93qPkdnPClJbkUHWMnZRv+jrXwgpIjTcDcRNHx
Xt4WIIbX9TfGywfPIoghJZ+XTcADsY814p38mX3yB+V6VHk6LR7M3mJ7Yzuu1DSi
fmXoQs0mhw1fWVKfkNhQJot5fqpCDJAIT4tSxufjhDK/ty0ggxD6ua1zK8L+nly+
tEwn55s5zFQjebdO4jnizn+Boj6vg9MXOXeUlEv2N8dQI5VutvjV5S8jy900ls1g
hIGUn4PO7wkVPnWYCdwxHn61wEdWIXZBmS2LMOmDjBwIQGGAFMmhJ+qBvxNWTS3O
qED0MFVfx57oluJmuQINBF64+akBEADazTFTPssp7zCfw9cKTZhcrbd5ZTvWr7WS
YJ5HmTvXFjz7tJA9vfGFzLRqqG42fDhIrTZEXITxkoLYHcl6VD9ECVTmvB9wnnnX
Bdu1TiYFs9vilE5CiyttHEbDam9Ralwd7p0GgxCXV4nQq7t4LTqa6VZL66H/PWut
dXdYVlZldNBk6lkx5PYzKRglI+hNtmebdRvh6tFNNYv5lU2kDAQUr5JptXIzmY5N
hrUtT0cOnBQLUurVqTtRqssko9Dhl9YKa5zucBJCGz+vHfVL9mWMdb0M+vZKvZtV
EqJ20e0obqc+SgzP0srRb1NWvGB3s3vzVqBcp6H27PA0RaEXwTzj5/AvN20D/pVA
DHl7eJi//7xwB2wL6tDe8xFRe2T4pAtjBOvE7xQqomwpu66O/sHpqkS642e61aDO
goXOqvf9o8w8CsqZroeMDnH0jZyL+4stb01o+NaQMmggpbI4ZNFSxljVbuSu0K0L
3mb7lL4LeeEJv9a+EEHQJgch300Vtqi6vQLLYwF1x+swN6+kmRHY8X4nNvzlVDdQ
H3vyWFs4MA9rJyA0S0Von7jvhdo0IofSFsjRAALD5uWUJv/Xe3thB2eP2TorH1Kz
xDcr7iHPixv3PwK3O5/UBcDL0O+Xsdql0PONUkc1Sl7Xs4DRfJ+CB2YpBbblRIA5
zHBtf9HpywARAQABiQI8BBgBCgAmFiEErY9oS0A4gFZSYL8+re6FxJPiAXEFAl64
+akCGwwFCQHhM4AACgkQre6FxJPiAXHScQ/9GpaeuwqcbBSUa2a9x0UFcgoELOwx
1ZBDM6LpQHzshB8GEXkCNGe0FCCkDsk217D2SUjJXBmjmuE6slI/LOQm6VEltTNn
1EXtLwL4iBtGV7RXRZpr/zJqjP3Jyjbk/O+v3hBcktkRuwYdIP/Lm7/E88w6hADq
ok2NeUKwEUeJfc8pOXjhhfAkxl5+LlKW8kKrZoezeb1ZH/iWuOcOCTQXvO3fjIZ4
ymntiDvzUVUVxd1WXkRn5Z/rXSgtEe0VL/lY9+Kz6Ykai8EDiYhHpg5f74RZQenC
/UnMCaJmqkQOzm6DZUcixeEYe+k6uBrfcNA7Rwd2bm5T0X3AFilHuVLpCSASW0Yk
um4mLSHu0jsf9wLwP9oPa3Xmove9FuG3f2Mqv1mVB6WC9+RWnut7Fm4HztxScR8q
sKDIIbcVbabsNXGLXLI1joMqdiR2smqlCIvrJpZaxr4BX8ETtbFIF1dkTQgFa8Oo
P5eUKdWKmv160PoPe6urQwSLGk4K34hGm32j+cYOcrKjIY/qze0cwLQJKMUYFAco
9flml1QVAufLl17qDludGd38gRRhkkUp0ZWe99sOuU4n9+8tmJk/UkV7V/FGn/q6
XCgZ4w8iPXpvj0bji9YdETCPbkagswo3q2WoeohCxUtT/FWU8MzFzH/rDrR5LRFs
5uwmqVmGAS/iiU8=
=qeBM
>

*****
