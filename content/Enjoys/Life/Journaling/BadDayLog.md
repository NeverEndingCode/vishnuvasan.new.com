---
title: "Leader vs Boss | Team Leader vs Manager"
date: 2020-05-20T21:29:56+05:30
showInMenu: false
tags: ["Leader vs Boss","Team Leader vs Manager"]
hideLastModified: true
draft: false
---
**4 Min Read** | May 20, 2020 10:39 PM

*****
I have always wondered why many of the Managers I have worked with are Total Effing Aholes.

Is it because of me or Is it because the World of Managers are like that?
Is it only me or Is it Everyone who thinks the same?

May be the Problem lies with Me?
May be I have to change the Perspective?
or May Be Not?

Everytime I sit with somebody to discuss this Every one of them I heard from say that they have been asked to change their attitude towards the Manager

Why the hell is it so? Why cant the Manager understands any one of us?
Why cant he act like a human or treat us like a human?
Why does he always think that he is the penultimate authority to do anything?

Recently I was reading a book from O Reilly called [Managing your Managers](https://www.oreilly.com/library/view/managing-your-manager/9780071751933/title.html). It was equally telling in a lot of perspective to change the way i think

But the most important question is a person born that way to live. Why are you people always telling someone to change his perspective everytime?

And by the time he gets old he may die without knowing how he/she should really be.

Rather just live for the moment eff him and go ahead with what it is.

It is quite confusing world.

But I have always struggled to find a good Leader. 
I have come across a very few of them but never really one who guided me to where I wanted to reach.

After some years of waiting, I rather realized that Let me be the Beacon for the Small Crowd rather Than getting old waiting for a Beacon!

Why everybody prefer a Leader over a Manager Always?
Is it like the quote below.( Excerpt )

> #### Being Selfless Despite Being Selfish!

Does the Title make sense grammatically in English? Who Cares. Because we are a generation of millennial questioning everything and making meaning for everything

We added the word [**Google**](https://google.com) in the Dictionary. In fact we have our own [**Urban Dictionary**](https://www.urbandictionary.com/) which no language acknowledges.

As a matter of fact Language is merely a collection of varied sound annotations which most of the people acknowledge or understand.

So I greatly believe that the oxymoron **"Being Selfless despite Being Selfish"** makes real sense in the sense of #Leadership

How can you be Selfless despite being Selfish? Sounds Tricky.

One Person who pops into my mind when i think of this phrase is [**Sundar Pichai**](https://en.wikipedia.org/wiki/Sundar_Pichai) . And that is what the googlers think about him too.

His Ladder of Success is far far varied from the traditional boring success stories of every corporate.

I greatly believe that some of the quality of Great Leadership are Empathy and Selflessness

Selflessness is you feeling the success without you actually being part of the day to day work.

Taking the success silently and standing a mile apart and watching them celebrate and having a world of celebration within silently

You losing your ego and uniting yourself as a team instead of being an individual irrespective of all your achievements and intelligence quotient.

The same thing what [**Zlatko Dalic**](https://en.wikipedia.org/wiki/Zlatko_Dali\%C4\%87) did when Croatia won [Semifinals](https://en.wikipedia.org/wiki/2018_FIFA_World_Cup_knockout_stage\#Croatia_vs_England) against England. It needs great sense of maturity, composure and most importantly "Selflessness" we are talking about. But every croatian player was chanting Dalic's Name. That Moment, That Maturity, That Success it defines the Leader and the Title. What a Team! And What a Man!

Success of a Leader is not in the Numbers they achieve rather it is in the People they earned who will remember them for their lifetime.

> Like the Sugar in the Coffee. We always praise the Coffee never the sugar.

Celebrating the success of your team/group/organization as your success but being a silent enabler. And Repeatedly doing it without expecting anything. It sounds illogical especially in our corporate world where everything is driven by numbers, monetary benefits, designation etc.,

But it is a blissful emotion. This feeling is worth the heaven. And you either have to have it naturally or develop it over time. Not only in an organization . It is the same feeling and sense of accomplishment you feel when your daughter graduates without you writing the exams. But your daughter knows that you did everything and even the world knows it too.

Be Selfless despite being Selfish!

A Leader is best when people barely know he exists, when his work done, his aim fulfilled, they will say: we did it ourselves - [ **Lao Tzu :) ** ](https://en.wikipedia.org/wiki/Laozi)
