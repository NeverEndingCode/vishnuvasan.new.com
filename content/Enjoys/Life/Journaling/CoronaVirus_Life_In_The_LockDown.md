---
title: "COVID 19 - Life in the LockDown"
date: 2020-05-09T14:59:13+05:30
showInMenu: false
tags: ["Covid19","CoronaVirus"]
hideLastModified: true
draft: false
---

# Covid 19 - Life in the LockDown - @ Bangalore, India
###### May 10, 2020 12:15 PM | **3 Min Read**

*****

> [Covid 19](https://en.wikipedia.org/wiki/Coronavirus_disease_2019)
>
> **Unanimous Buzz Word of Every Human Alive in 2020**<br>
>
> *A Human Nightmare but a Heavenly Dream for Mother Earth to Replenish its [AutoPoeisis](https://en.wikipedia.org/wiki/Autopoiesis)*

[Who](https://en.wikipedia.org/wiki/World_Health_Organization) in the World would have thought such a pandemic was about to get unleashed and torment the Last 100 Year Human History.

Coronavirus is shaking the [World](https://www.worldometers.info/coronavirus/), [Economies](https://en.wikipedia.org/wiki/2020_stock_market_crash),Nations, [Leaders](https://www.nbcnews.com/politics/2020-election/chaotic-disaster-obama-hits-trump-s-coronavirus-response-warns-disinformation-n1203806), [Families](https://www.nytimes.com/2020/03/16/world/europe/italy-coronavirus-funerals.html) and Literally [Every other Walk Of Life](https://www.marketwatch.com/story/coronavirus-update-us-death-toll-tops-75000-as-economy-sheds-record-setting-205-million-jobs-2020-05-08) with close to 3,00,000 Casualties


The DoomsDay Effect it brought to our Lives has been astounding and devastating. As they Say

> Every Event Has a Reason.
>
> Every Reason Has an Intent.
>
> Every Intent has a Long Lasting Effect.

We have lost 1000's of Libraries of Human History as like the African Saying

> **One Whole Library is scorched when an Elderly Departs!**


| Effects | How we Evolved?	| Personal LifeStyle @ Bangalore |
| :--------------------------- | :-------------------------------------------------------- | :------------------- |
| Thousands of our Beloved Left us | Heart Goes to Everyone who lost their loved ones and I hope our memories and time shall help us. We kept our Hopes Alive and our Healthcare Professionals showed the world [Humanity](https://en.wikipedia.org/wiki/Humanity_\(virtue\)).World Is in LockDown. We have started to Live with our Home Becoming our World and badly hoping for a better tomorrow | Locked in Home since 10th March.
| Contamination Through Human Liquids | Masks has become our New Norm and [OCD](https://en.wikipedia.org/wiki/Obsessive–compulsive_disorder) to be our New Habit. | Severe [OCD](https://en.wikipedia.org/wiki/Obsessive–compulsive_disorder) Development. Unavailability of Daily Essentials, Food, Groceries due to [Panic Buying](https://en.wikipedia.org/wiki/Panic_buying)
| Loss of Jobs | Governments are bailing out the unemployed. [Gig Economy](https://en.wikipedia.org/Gig_worker) Facing the brunt of it | Thankfully I lost my Hikes, Appraisals over the Job. But Working from Home has been pathetic with Sick Indian MicroManagers.
| Work From Home Culture | People who had Jobs had to work from Home to survive. And Numerous tips online started on [WFH Tips](https://www.carryology.com/insights/insights-1/staff-picks-work-from-home-essentials/) | Read a Few and trying to stick to the tips for better efficiency and productivity
| Relationship | Dating has been never more [weirder](https://edition.cnn.com/2020/03/18/health/dating-during-coronavirus-wellness-trnd/index.html). Raise of [VideoCalling](https://medium.com/digital-literacy-for-decision-makers-columbia-b/the-rise-and-fall-of-zoom-ecfcc1656605) has never been so steep | Personally not a fan of video calling. so never really used it |
| World | Mother Earth went to her roots and started [Healing](https://worldofbuzz.com/mother-earth-successfully-healed-itself-from-1-million-km-hole-in-ozone-layer/) herself. Lesser [CO2 Emissions](https://www.bbc.com/news/science-environment-51944780), Ozone Hole Heal and the [unexplored](https://yourstory.com/socialstory/2020/04/coronavirus-lockdown-environment-air-pollution-covid-19) | Never Believed Bangalore had so much of [Fresh Air and Blue Skies](https://www.youtube.com/watch?v=5CVXZUMLj6M)|


We still have to see how the ATMs,Facial Recognition Technologies, Mere Greeings are gonna get adapted to these changes.

As I started, these are very few things where we evolved in time and situation out of the myriad in mind and life.


> Hope atleast now we shall start to feel our [World](https://www.politico.com/news/magazine/2020/03/19/coronavirus-effect-economy-life-society-analysis-covid-135579),[Environment](https://en.wikipedia.org/wiki/Greta_Thunberg),[People](https://www.thefamouspeople.com/human-rights-activists.php),Society,Ethnicity,Culture to be our Treasured Home to be taken with extreme care rather than discriminating on Economical,Political,Racial and all the unknown Grounds

###### #Thank God for Making us More United and One than ever Before.
## #Let God Bless us All!

